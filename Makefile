hymnbook.pdf : firstlines.ind hymnbook.tex gabc_obj kyriale_obj
	lualatex hymnbook.tex

firstlines.ind : firstlines.idx 
	makeindex -s myright.ist firstlines.idx

gabc_obj : 
	cd gabc; make all

kyriale_obj : 
	cd kyriale; make all

hymnbook.ps : hymnbook.pdf
	pdftops hymnbook.pdf


a5book.ps : hymnbook.ps
	psbook -s4 hymnbook.ps | pstops "2:0L@.92(21.1cm,8.3mm)+1L@.92(21.1cm,14.767cm)" > a5book.ps

twoa5books.ps : hymnbook.ps
	pstops "1:0L@.92(21.1cm,8.3mm)+0L@.92(21.1cm,14.767cm)" hymnbook.ps > twoa5books.ps

fitintoa5case.ps : hymnbook.ps
	pstops "2:0L@.88(205.58mm,150mm)+0R@.88(4.42mm,150mm),1L@.88(205.58mm,14.39mm)+1R@.88(4.42mm,282.61mm)" hymnbook.ps > fitintoa5case.ps

foura6books.ps : 
	pstops "2:0@0.64(4mm,1.7mm)+0@0.64(4mm,148mm)+0@.64(105mm,1.7mm)+0@.64(105mm,148mm),1@.64(7.5mm,-0.3mm)+1@.64(7.5mm,146mm)+1@.64(108.5mm,-0.3mm)+1@.64(108.5mm,146mm)" hymnbook.ps > 4a6.ps

gutteredge.ps : hymnbook.ps
	pstops "2:0L@0.88(205.58mm,3mm)+0R@0.88(4.42mm,297mm),1L@.88(205.58mm,160.9mm)+1R@.88(4.42mm,134.1mm)" hymnbook.ps > book.ps

nbook.ps : hymnbook.pdf
	pdftops hymnbook.pdf
	psbook -s4 hymnbook.ps | pstops "2:0L@0.8(196.44mm,28.08mm)+1L@0.8(196.44mm,150mm)" > nbook.ps

fanbook.ps : hymnbook.pdf
	pdftops hymnbook.pdf
	pstops "1:0L@0.8(196.44mm,27.08mm)+0L@0.8(196.44mm,151mm)" hymnbook.ps > fanbook.ps

Gaudete_obj :
	cd Gaudete; make all

Advent3.pdf : Advent3.tex Gaudete_obj kyriale_obj gabc_obj
	lualatex Advent3.tex
	
