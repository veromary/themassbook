\babel@toc {latin}{}
\babel@toc {british}{}
\contentsline {chapter}{Contents}{1}%
\contentsline {chapter}{\chapternumberline {1}At Exposition}{2}%
\contentsline {chapter}{\chapternumberline {2}Prayers to begin Adoration}{6}%
\contentsline {chapter}{\chapternumberline {3}Benediction}{7}%
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\contentsline {chapter}{\chapternumberline {4}After Benediction}{13}%
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\contentsline {chapter}{\chapternumberline {5}Eucharistic Hymns}{17}%
\contentsline {chapter}{\chapternumberline {6}Other Hymns}{25}%
\contentsline {chapter}{\chapternumberline {7}Prayers}{32}%
\contentsline {section}{Forty Hours Devotion}{32}%
\babel@toc {latin}{}
\babel@toc {british}{}
\contentsline {section}{Litany of the Holy Name of Jesus}{43}%
\contentsline {section}{Litany of the Sacred Heart of Jesus}{47}%
\contentsline {section}{Indulgenced prayers to the Sacred Heart}{51}%
\contentsline {section}{Litany of the Blessed Virgin Mary}{57}%
\contentsline {section}{Litany of St.~Joseph}{61}%
\contentsline {section}{Litany of St.~Peter}{64}%
\contentsline {subsection}{Prayer of the Confraternity of St.~Peter}{65}%
\contentsline {section}{Novena to St.~Anthony of Padua}{66}%
\contentsline {section}{Prayer against drought}{67}%
\contentsline {section}{Prayer in times of Epidemics}{68}%
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\babel@toc {latin}{}
\babel@toc {british}{}
\contentsline {section}{Litany of Humility}{70}%
\contentsline {chapter}{Index}{71}%
